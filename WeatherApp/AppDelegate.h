//
//  AppDelegate.h
//  WeatherApp
//
//  Created by Sword Software on 29/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

