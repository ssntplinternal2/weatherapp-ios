//
//  ViewController.m
//  WeatherApp
//
//  Created by Sword Software on 29/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *imageNameArray;
    
}

@property (weak, nonatomic) IBOutlet UITableView *table;

@end



@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.table registerClass:[UITableViewCell self] forCellReuseIdentifier:@"Cell"];
    NSMutableArray *names = [[NSMutableArray alloc] init];
    
    
    maxTempArray = [[NSMutableArray alloc]init];
    minTempArray = [[NSMutableArray alloc]init];
    dayArray = [[NSMutableArray alloc]init];
    
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.darksky.net/forecast/e6d42b5f6c79dbeccfcc7d600d6b6462/28.693362,77.171911"];
    
    NSError *error;
    NSURLResponse *responses;
    NSData * data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL: [NSURL URLWithString:urlString]] returningResponse:&responses error:&error];
    //NSLog(@"%@",data);
    
    if (data) {
        NSData *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        //NSLog(@"%@",jsonData);
        NSData *currentData = [jsonData valueForKey:@"currently"];
        
        NSString *temperature = [currentData valueForKey:@"temperature"];
        NSString *apparentTemperature = [currentData valueForKey:@"apparentTemperature"];
        
        float celsius = ((temperature.floatValue)-32.0)*(5.0/9.0);
        float apparentcelsius = ((apparentTemperature.floatValue)-32.0)*(5.0/9.0);
        
        [names addObject:[NSString stringWithFormat: @"Time Zone :- %@",[jsonData valueForKey:@"timezone"]]];
        [names addObject:@"\t\t\tCurrently :-"];
        [names addObject:[NSString stringWithFormat: @"%@",[currentData valueForKey:@"summary"]]];
        [names addObject:[NSString stringWithFormat: @"Overall:- %@",[currentData valueForKey:@"icon"]]];
        [names addObject:[NSString stringWithFormat: @"Temperature:- %f",celsius]];
        [names addObject:[NSString stringWithFormat: @"Apperent Temperature:- %f",apparentcelsius]];
        [names addObject:@""];
        [names addObject:@"This Week :-"];
        
        NSData *dayData = [jsonData valueForKey:@"daily"];
        NSArray *dataArray = [dayData valueForKey:@"data"];
        
        NSData *current = [NSDate date];
        NSInteger weekday = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:current]weekday];
        
        for (int i = 0; i< dataArray.count; i++) {
            NSData *tmpData = [dataArray objectAtIndex:i];
            
            NSString *minText = [tmpData valueForKey:@"temperatureMin"];
            NSString *maxText = [tmpData valueForKey:@"temperatureMax"];
            
            NSString *minValue = [self getCelsius:minText.floatValue];
            NSString *maxValue = [self getCelsius:maxText.floatValue];
            
            [minTempArray addObject:minValue];
            [maxTempArray addObject:maxValue];
            
            NSString *dailySummary = [tmpData valueForKey:@"summary"];
            NSString *dailyIcon = [tmpData valueForKey:@"icon"];
            
            if (weekday>7)
                weekday = weekday-7;
            
            NSString *weekdayName;
            switch (weekday) {
                case 1:
                    weekdayName = @"Sunday";
                    break;
                case 2:
                    weekdayName = @"Monday";
                    break;
                case 3:
                    weekdayName = @"Tuesday";
                    break;
                case 4:
                    weekdayName = @"Wednesday";
                    break;
                case 5:
                    weekdayName = @"Thursday";
                    break;
                case 6:
                    weekdayName = @"Friday";
                    break;
                case 7:
                    weekdayName = @"Saturday";
                    break;
                    
                default:
                    break;
                    
            }
            weekday++;
            
            [names addObject:[NSString stringWithFormat: @"\t\t\t%@",weekdayName]];
            [names addObject:[NSString stringWithFormat: @"%@",dailySummary]];
            [names addObject:[NSString stringWithFormat: @"Overall:- %@",dailyIcon]];
            [names addObject:[NSString stringWithFormat: @"Minimum Temperature:- %@",minValue]];
            [names addObject:[NSString stringWithFormat: @"Maximum Temperature:- %@",maxValue]];
        }
    }
    else {
    }
    imageNameArray = [names copy];
}

-(NSString *)getCelsius:(float)fahrenheit {
    NSString *returnString = [NSString stringWithFormat:@"%.1f",(fahrenheit -32.0)*(5.0/9.0)];
    return returnString;
}

#pragma mark - UITableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [imageNameArray count];
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    cell.textLabel.text = imageNameArray[indexPath.row];
    cell.textLabel.numberOfLines = 3;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    return cell;
}





@end



